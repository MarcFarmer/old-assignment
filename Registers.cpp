#include <iostream>

#include "Registers.h"
#include "Global.h"

Registers::Registers(): m_pc(0) {
	for (int i = 0; i < 32; i++) {
		m_registers[i] = 0;
	}
}

void Registers::decrementPc() {
	m_pc -= 4;
}

unsigned int Registers::getPc () {
	return m_pc;
}

int Registers::getValue (unsigned int n) {
	if (n >= 0 || n <= 31) {
		return m_registers[n];
	}
	if (g_verbose) {			// invalid n
		std::cout << "Error: Cannot get value from register outside of range [0,31]. Returning 0." << std::endl;
		return 0;
	}
}

void Registers::incrementPc () {
	m_pc += 4;
}

void Registers::setPc(unsigned int address) {
	while (address % 4 != 0)	// round down to nearest aligned address
		address--;
	m_pc = address;
}

void Registers::setValue (unsigned int n, int value) {
	if (n >= 1 && n <= 31) {
		m_registers[n] = value;
	} else if (g_verbose) {
		std::cout << "Error: Cannot set value in register outside of range [1,31]. Given register #: " << n << std::endl;
	}
}
