#ifndef BLOCK_H
#define BLOCK_H

class Block {

	private:
		unsigned int m_baseAddr;		// first word address
		unsigned int m_blockNum;		// for convenience, store baseAddr/4096, to give the "block number" if all blocks were in ordered sequence
		unsigned int m_blockSize;		// size of block in 32-bit words
		int* m_words;				// array of words
		bool addrInBlock(unsigned int addr);	// helper/safety function: true if address is in this block

	public:
		Block(unsigned int baseAddr, unsigned int blockSize);	// blockSize in 32-bit words
		~Block();						
		unsigned int getBlockNum();
		unsigned int getBlockSize();
		int getWord(unsigned int addr);
		void setWord(unsigned int addr, int value);

};

#endif