#include "Block.h"
#include "Memory.h"

// use binary search on m_blocks to find index of block containing addr
// if the block has not yet been created, create it in correct sorted position

Memory::Memory(unsigned int blockSize) {
	m_blockSize = blockSize;
}

Memory::~Memory() {
	for (int i = 0; i < m_blocks.size(); i++) {
		delete m_blocks[i];
	}
}

unsigned int Memory::getBlockIndex(unsigned int addr) {

	// if no Blocks have been created, create block at index 0
	if (m_blocks.empty()) {
		Block *b = new Block((addr/4096)*4096,m_blockSize);	// addr/4096 = block number, multiply by 4096 for base addr
		m_blocks.push_back(b);
		return 0;
	}

	// search for block that contains the memory address "addr"
	// use binary search to search through member variable m_blocks, using block number (addr/4096) as the key
	unsigned int left = 0;
	unsigned int right = m_blocks.size() - 1;
	unsigned int key = addr/4096;

	while (right > left) {
		int middle = left + ((right-left)/2);
		if (m_blocks[middle]->getBlockNum() == key) {
			return middle;
		} else if (m_blocks[middle]->getBlockNum() > key) {
			right = middle - 1;
		} else {
			left = middle + 1;
		}
	}

	if (m_blocks[left]->getBlockNum() == key) {
		return left;
	}

	// block containing address "addr" was not found, create it at the correct index to keep Blocks in sorted order, return this index
	// std::vector::insert method adds element to vector before the given index
	Block *b = new Block(addr,1024);
	if (key < m_blocks[left]->getBlockNum()) {	// if blockNum of new block < blockNum of block indexed by "left" and "right", create new block before existing block
		m_blocks.insert(m_blocks.begin() + left,b);
		return left;
	} else {					// create new block after existing block
		if (left+1 == m_blocks.size()) {		// if we need to create block after last block, can't use insert method
			m_blocks.push_back(b);
			return left+1;
		} else {
			m_blocks.insert(m_blocks.begin() + left + 1,b);
			return left+1;
		}
	}

	return 0;

}

unsigned int Memory::getValue(unsigned int addr) {
	while (addr % 4 != 0)	// round down to nearest aligned address
		addr--;
	unsigned int blockIndex = getBlockIndex(addr);
	return m_blocks[blockIndex]->getWord(addr);
}

void Memory::setValue(unsigned int addr, int value) {
	while (addr % 4 != 0)	// round down to nearest aligned address
		addr--;
	unsigned int blockIndex = getBlockIndex(addr);
	m_blocks[blockIndex]->setWord(addr,value);
}
