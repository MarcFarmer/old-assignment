#ifndef GLOBAL_H
#define GLOBAL_H

// true if -v cmd line option is given
extern bool g_verbose;

// counts of instruction types and cycles to be printed in Driver
extern unsigned int g_add;
extern unsigned int g_addi;
extern unsigned int g_beq;
extern unsigned int g_bne;
extern unsigned int g_j;
extern unsigned int g_lw;
extern unsigned int g_slt;
extern unsigned int g_slti;
extern unsigned int g_sw;
extern unsigned int g_cycles;

#endif
