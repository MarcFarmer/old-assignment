#ifndef MEMORY_H
#define MEMORY_H

#include <vector>

class Memory {

	private:
		std::vector<Block*> m_blocks;
		unsigned int m_blockSize;
		unsigned int getBlockIndex(unsigned int addr);		// helper: find index of block that contains address addr. if it does not exist yet, create it at the index where it should be, and return its index

	public:
		Memory(unsigned int blockSize);				// blockSize in 32-bit words
		~Memory();
		unsigned int getValue(unsigned int addr);
		void setValue(unsigned int addr, int value);

};

#endif
