#ifndef REGISTERS_H
#define REGISTERS_H

class Registers {

	private:
		int m_registers[32];
		unsigned int m_pc;

	public:
		Registers();
		void decrementPc();	// useful for implementing jump instruction
		unsigned int getPc();
		int getValue(unsigned int n);
		void incrementPc();
		void setPc(unsigned int address);
		void setValue(unsigned int n, int value);

};

#endif
