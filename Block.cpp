#include <iostream>

#include "Block.h"
#include "Global.h"

Block::Block(unsigned int baseAddr, unsigned int blockSize) {
	// set member variables
	m_baseAddr = baseAddr;
	m_blockNum = baseAddr >> 12;		// baseAddr / 4096, used as search key when searching for a block
	m_blockSize = blockSize;
	m_words = new int[blockSize];
	// initialise all memory locations in block = 0
	for (unsigned int i = 0; i < blockSize; i++) {
		m_words[i] = 0;
	}
}

Block::~Block() {
	delete [] m_words;
}

unsigned int Block::getBlockNum() {
	return m_blockNum;
}

unsigned int Block::getBlockSize() {
	return m_blockSize;
}

int Block::getWord(unsigned int addr) {
	if (addrInBlock(addr)) {
		unsigned int offsetInBlock = addr % 4096;	// how many bytes above the base address of this block is the given address?
		return m_words[offsetInBlock >> 2];		// offset in bytes/4 = offset in 32 bit words
	} else if (g_verbose) {
		std::cout << "Error: Memory location " << addr << " is not in this block. Returning 0." << std::endl;
		return 0;
	}
}

void Block::setWord(unsigned int addr, int value) {
	if (addrInBlock(addr)) {
		unsigned int offsetInBlock = addr % 4096;	// how many bytes above the base address of this block is the given address?
		m_words[offsetInBlock/4] = value;		// offset as bytes / 4 = offset as words
	} else if (g_verbose) {
		std::cout << "Error: Memory location " << addr << " is not in this block. Cannot set value." << std::endl;
	}
}

bool Block::addrInBlock(unsigned int addr) {
	if (addr >> 12 == m_baseAddr >> 12) {
		return true;	// addr is in this block
	}
	return false;
}