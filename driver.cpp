#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>	// convert string representing hex to unsigned int
#include <iomanip>	// precision of floats

#include <string.h>	// for strtok
#include <stdio.h>

#include "Block.h"	// represents block of memory
#include "Memory.h"	// handles access to memory (blocks), creation of blocks
#include "Registers.h"
#include "Global.h"

using namespace std;

/* --- MAIN - check for cmd line options, get input from stdin, parse input, print counts to conclude program */

// given a command in a string, determine which command it is, call function to execute it
void lineParser(string line);

// Create memory and registers representations
static Memory m = Memory(1024);		// memory with block size = 1024 words
static Registers r = Registers();

int main(int argc, char* argv[]) {

	// Check for cmd line arg
	if (argc == 2 && static_cast<string>(argv[1]) == "-v") {
		g_verbose = true;	// set global variable to enable debugging output
		cout << "Debug messages enabled." << endl;
	}

	// Parse commands; get input from stdin
	string line;
	while (cin) {
		getline(cin,line);
		if (line.length() > 0) {
			lineParser(line);
		}
	}

	// Print counts after executing commands
	cout << "add: " << g_add << endl;
	cout << "addi: " << g_addi << endl;
	cout << "slt: " << g_slt << endl;
	cout << "slti: " << g_slti << endl;
	cout << "beq: " << g_beq << endl;
	cout << "bne: " << g_bne << endl;
	cout << "lw: " << g_lw << endl;
	cout << "sw: " << g_sw << endl;
	cout << "j: " << g_j << endl;
	unsigned int instructions = g_add + g_addi + g_beq + g_bne + g_j + g_lw + g_slt + g_slti + g_sw;
	cout << "Instruction count: " << instructions << endl;
	cout << "Cycle count: " << g_cycles << endl;
	if (instructions > 0 && g_cycles > 0) {
		cout.precision(4);
		cout << "Average CPI: " << fixed << static_cast<float>(g_cycles)/static_cast<float>(instructions) << endl;
	}

return 0;
}

/* --- PARSING LINES */

// for each command X, there is a function cmd_X to execute it, accessing memory and registers
void cmd_rn(const vector<string> &tokens);
void cmd_m(const vector<string> &tokens);
void cmd_pc(const vector<string> &tokens);
void cmd_execInst(const vector<string> &tokens);

void lineParser(string line) {
	vector<string> tokens;
	char *str = new char[line.length() + 1];		// must create a char[] version of string line, so it can be a strtok parameter
	for (int i = 0; i < line.length(); i++) {	// copy chars in string to char[]
		str[i] = line[i];
	}
	str[line.length()] = '\0';
	// tokenise char[] version of line into tokens vector
	char * p;
  	p = strtok (str," =");
	while (p != NULL && p[0] != '#') {		// stop tokenising at NULL (end of string) or # (start of comment)
		tokens.push_back(p);
		p = strtok (NULL, " =");
  	}

	if (g_verbose)
		cout << "line length " << line.length() << endl;

	if (tokens.empty())
		return;

	// examine first token, execute command
	if (tokens[0][0] == 'r') {		// rn, rn = value
		cmd_rn(tokens);
	} else if (tokens[0][0] == 'm') {	// m address, m address = value
		cmd_m(tokens);
	} else if (tokens[0][0] == 'p') {	// pc, pc = address
		cmd_pc(tokens);
	} else if (tokens[0][0] == '.') {	// ., . n
		cmd_execInst(tokens);
	} else if (g_verbose) {		// got >= 1 token, unrecognised command
		cout << "Error: Non-empty line did not contain known command: " << line << endl;
	}

delete str;	// deallocate char array that was needed for strtok
}

/* --- EXECUTING COMMANDS */

// convert string representing hex to unsigned int
unsigned int hexStr_UnsInt (string hexStr) {
	stringstream ss;
	unsigned int hexValue;
	ss << hex << hexStr;
	ss >> hexValue;
	return hexValue;
}

// convert string representing hex to signed int
int hexStr_Int (string hexStr) {
	char * p;
	long n = strtoul(hexStr.c_str(), &p, 16);
	return static_cast<int>(n);
}

// print signed int as hex, sign extended to 8 digits (lowercase)
void printSignExtendedHex(int value) {
	char padChar;
	if (value < 0)
		padChar = 'f';
	else
		padChar = '0';

	cout << setfill(padChar) << setw(8); 
	cout << hex << value << dec << endl;
}

// execute commands:
// rn
// rn = value
void cmd_rn(const vector<string> &tokens) {
	string str_n = tokens[0].substr(1,tokens[0].length() - 1); // register #: substring from tokens[0][1] to tokens[0][token length - 1]
	int n = atoi(str_n.c_str());	// convert str_n to integer

	if (tokens.size() == 1) {	// show content of register n in hex
		printSignExtendedHex( r.getValue(n) );
	} else {			// set content of register n
		r.setValue(n,hexStr_Int(tokens[1]));
	}
}

// execute commands:
// m address
// m address = value
void cmd_m(const vector<string> &tokens) {
	if (tokens.size() == 2) {	// print value at address
		printSignExtendedHex( m.getValue(hexStr_UnsInt(tokens[1])) );
	} else {			// set value at address
		m.setValue(hexStr_UnsInt(tokens[1]),hexStr_Int(tokens[2]));
	}
}

// execute commands:
// pc
// pc = address
void cmd_pc(const vector<string> &tokens) {
	if (tokens.size() == 1) {	// print PC in hex
		printSignExtendedHex( r.getPc() );
	} else {			// set PC to address given in hex
		r.setPc(hexStr_UnsInt(tokens[1]));
	}

}

// execute commands:
// .
// . n
void cmd_execInst(const vector<string> &tokens) {

	// determine number of instructions to execute
	unsigned int instCount;
	if (tokens.size() == 1) {
		instCount = 1;
	} else {
		instCount = atoi(tokens[1].c_str());
	}

	// execute each instruction by fetching from and incrementing PC
	for (int inst = 1; inst <= instCount; inst++) {
		unsigned int machineCode = m.getValue(r.getPc());	// fetch: get value from memory at address stored in PC
		r.incrementPc();
		if (g_verbose)
			cout << "pc after increment: " << r.getPc() << endl;
		unsigned int opcode = machineCode >> 26;		// high order 6 bits

		// check opcode to determine instruction type, then extract common fields

		// Instruction type: R-type
		if (opcode == 0x00) {
			unsigned int funct = machineCode & 0x3F;	// low order 6 bits
			unsigned int d,s,t;				// R-Type inst fields
			d = (machineCode >> 11) & 0x1F;
			t = (machineCode >> 16) & 0x1F;
			s = (machineCode >> 21) & 0x1F;
			// check funct to determine which R-Type instruction
			if (funct == 0x20) {		// add
				r.setValue(d,r.getValue(s)+r.getValue(t));	// rd = rs + rt
				g_add++;
				g_cycles += 4;
			} else if (funct = 0x2A) {	// slt
				if (r.getValue(s) < r.getValue(t))		// rd = 1 if rs < rt
					r.setValue(d,1);
				else
					r.setValue(d,0);
				g_slt++;
				g_cycles += 4;
			} else if (g_verbose) {
				cout << "Error: Have R-Type inst (opcode = 0x00), but unknown funct: " << hex << funct << dec << endl;
			}

		// Instruction type: J-Type
		} else if (opcode == 0x02) {	// j
			r.decrementPc();					// j doesn't increase PC by 4, undo decrement
			int immediate = (machineCode << 6) >> 6;		// low order 26 bits
			unsigned int pcFirst4 = r.getPc() & 0xF0000000;		// keep first 4 bits of PC
			unsigned int newPc = pcFirst4 + (immediate << 2);	// append 28 bits of (26 bit signed constant) * 4
			r.setPc(newPc);
			g_j++;
			g_cycles += 2;

		// Instruction type: I-Type
		} else if (opcode >= 0x04 && opcode <= 0x2B) {		// range of implemented I-Type instructions			
			int immediate;
			unsigned int s,t;
			immediate = machineCode & 0x0000FFFF;
			if ((immediate >> 15) & 0x1 == 0x1)		// no arithmetic shift operator, so sign extend by checking highest order bit
				immediate = immediate | 0xFFFF0000;
			t = (machineCode >> 16) & 0x1F;
			s = (machineCode >> 21) & 0x1F;
			if (opcode == 0x08) {	// addi
				r.setValue(t, r.getValue(s) + immediate);	// rt = rs + i
				if (g_verbose)
					cout << hex << "@@@ addi instr. reg: " << r.getValue(s) << " imm: " << immediate << dec << endl;
				g_addi++;
				g_cycles += 4;
			} else if (opcode == 0x04) {	// beq
				if (r.getValue(s) == r.getValue(t))		// if rs == rt, add offset*4 (immediate) to PC
					r.setPc(r.getPc() + (immediate << 2));
				g_beq++;
				g_cycles += 3;
			} else if (opcode == 0x05) {	// bne
				if (r.getValue(s) != r.getValue(t))		// if rs != rt, add offset*4 (immediate) to PC
					r.setPc(r.getPc() + (immediate << 2));
				g_bne++;
				g_cycles += 3;
			} else if (opcode == 0x23) {	// lw
				int memLocation = (r.getValue(s) + immediate);	// load value from mem location i + rs into rt
				r.setValue(t, m.getValue(memLocation));
				g_lw++;
				g_cycles += 5;
			} else if (opcode == 0x0A) {	// slti
				if (r.getValue(s) < immediate)			// if rs < i, rt = 1. else rt = 0
					r.setValue(t,1);
				else
					r.setValue(t,0);
				g_slti++;
				g_cycles += 4;
			} else if (opcode == 0x2B) {	// sw
				int memLocation = (r.getValue(s) + immediate);	// store rt in mem location rs + i
				m.setValue(memLocation,r.getValue(t));
				g_sw++;
				g_cycles += 4;
			} else if (g_verbose) {
				cout << "Error: Unknown opcode: " << hex << opcode << dec << endl;
			}
		} else if (g_verbose) {
			cout << "Error: Unknown opcode: " << hex << opcode << dec << endl;
		}
	}
}
