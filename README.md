Input: Text file that contains assembly language commands

Main file: driver.cpp
Parse input file to get assembly language commands
Simulate executing the commands, using C++ classes that represent memory and registers