all: driver.o Block.o Global.o Memory.o Registers.o
	g++ driver.o Block.o Global.o Memory.o Registers.o -o mipsim

Block.o: Block.cpp Block.h
	g++ -c Block.cpp

Global.o: Global.cpp Global.h
	g++ -c Global.cpp

Memory.o: Memory.cpp Memory.h
	g++ -c Memory.cpp

Registers.o: Registers.cpp Registers.h
	g++ -c Registers.cpp

driver.o: driver.cpp
	g++ -c driver.cpp

clean:
	rm *.o
